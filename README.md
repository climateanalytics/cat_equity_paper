# Fair National Greenhouse Gas Reduction Targets Under Multiple Equity Perspectives - A Synthesis Framework

This repository contains the scripts used to generate the figures underlying the paper titled "Fair National Greenhouse Gas Reduction Targets Under Multiple Equity Perspectives - A Synthesis Framework". Input data is available upon reasonable request - please address requests to gaurav.ganti@climateanalytics.org

### Authors: 

Gaurav Ganti, Andreas Geiges, Louise Jeffery, Matthew Gidden, Niklas Höhne, Bill Hare, Deborah Ramalope, Michiel Schaeffer, Hanna Fekete 

### Submitted to: 

Climatic Change

### Submission date: 

31.03.2021

### Files

* ```climatic_change.mplstyle```: Custom matplotlib configuration file for plot formatting
* ```analysis```: This folder contains the analysis scripts
* ```utils.py```: Helper utilities 
