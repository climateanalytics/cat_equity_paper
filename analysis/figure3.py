#!/usr/bin/env python
# coding: utf-8

# In[105]:


import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
import os 
import string

plt.style.use('../climatic_change.mplstyle')
# # Read in the study data

# In[11]:


study_data = pd.read_csv(
    os.path.join('..','data','cat_output','input_equity_210216.csv'),
    index_col = 0
)


# # Function to compute ECDF

# In[13]:


def compute_weighted_percentile(data, perc, weights):
    perc = perc
    ix = np.argsort(data)
    data = data[ix]
    weights = weights[ix]
    cdf = (np.cumsum(weights) - 0.5 * weights) / np.sum(weights)
    result = np.interp(perc, cdf, data)
    return result


# In[14]:


emi_col = 'Emissions_allocation_(MtCO2e)'


# # Create the clusters

# In[15]:


study_data.loc[:,'weight'] = None
study_data.loc[:,'Cluster'] = (
    study_data.loc[:, 'Category']
    +
    study_data.loc[:,'Stab_level_ID'] 
)


# # Select a country for illustration 
category_list = list(study_data.loc[:, 'Category'].unique())
cat_dict = dict()
for i in range(len(category_list)):
    cat_dict[category_list[i]] = i
# In[16]:


for iso in [
        'IND', 'EU27', 'ZAF', 'DEU',
        'ETH','UGA','TZA','SOM'
]:


    # In[17]:
    
    
    mask_iso = study_data.loc[:,'Country_ID'] == iso
    mask_year = study_data.loc[:,'Year'] == 2030
    
    
    # In[18]:
    
    
    data_iso = study_data.loc[mask_iso & mask_year,:]
    
    
    # In[20]:
    
    
    data_iso.head()
    
    
    # In[21]:
    
    
    for i_cluster, cluster in enumerate(study_data.Cluster.unique()):
        mask = data_iso.loc[:,'Cluster'] == cluster
        count_members = sum(mask)
        if count_members > 0:
            data_iso.loc[mask,'weight'] = 1/count_members
        else:
            data_iso.loc[mask,'weight'] = 0 
    
    
    # In[24]:
    
    
    data_iso.loc[:,'weight']=(
        data_iso.loc[:,'weight']  
        / 
        (data_iso.loc[:,'weight'].sum()) 
    ).astype(float)
    sort_df = data_iso.sort_values(emi_col)
    sort_df.loc[:,'cum_weight'] = sort_df.loc[:,'weight'].cumsum()
    sort_df.loc[:,'cum_non_weight'] = (sort_df.loc[:,'weight']*0+1).cumsum() / len(sort_df.loc[:,'weight'])
    sort_df.index = range(len(sort_df.index))
    
    
    # In[118]:
    
    
    get_ipython().run_line_magic('matplotlib', 'notebook')
    fig, ax = plt.subplots(2,1, sharex=True, num=1, figsize=[12,6])
    # ax[0].cla()
    # ax[1].cla()
    plt.subplots_adjust(left=0.4, right=0.95, top=0.94, bottom=0.06)
    
    # In[119]:
    
    
    # Adding the datapoints 
    cluster_list = list(study_data.Cluster.unique())
    cluster_list.sort()
    
    
    # In[120]:
    # plt.cla()
    
    for cluster in cluster_list:
        mask = data_iso.loc[:,'Cluster'] == cluster
        count = sum(mask)
        
        if '400' in cluster:
            color = 'xkcd:bluish'
            marker = 's'
        else:
            color = 'xkcd:bluish'
            marker = 'o'
        if count > 0:
            category = list(data_iso.loc[mask,'Category'].unique())[0]
            i = cat_dict[category]
            ax[0].plot(
                data_iso.loc[mask,emi_col],
                [i] * sum(mask),
                'd',
                markersize = 20 * np.sqrt(1/count),
                color = color,
                fillstyle = 'none',
                marker = marker
            )
    
    
    # In[121]:
    
    
    cluster_labels = [
        x
        .replace('450_ppm', ' | 2°C')
        .replace('400_ppm',' | 1.5°C')
        .replace('Eq._cumulative_per_capita_emissions', 'Cum per capita emission') 
        for x in cluster_list
    ]
    ax[0].set_yticks(range(len(cat_dict)))
    ax[0].set_yticklabels(labels = cat_dict.keys())
    ax[0].set_yticks([x+.5 for x in range(len(cat_dict))], minor=True)
    ax[0].yaxis.grid(True, which='minor')
    
    
    # In[122]:
    
    
    ax[1].plot(sort_df.loc[:,emi_col], sort_df.loc[:,'cum_weight'], color='black')
    
    
    # In[123]:
    
    
    # Add min/max span in upper and line for lower 
    _min = sort_df.loc[:,emi_col].min()
    _max = sort_df.loc[:,emi_col].max()
    
    ax[1].axvline(_min, color='grey', linestyle='dashed', alpha = 0.5)
    ax[1].axvline(_max, color='grey', linestyle='dashed', alpha = 0.5)
    ax[0].axvspan(_min,_max, color='grey',alpha=0.2)
    
    
    # In[124]:
    
    
    # Added weighted percentile cuts 
    p5 = compute_weighted_percentile(sort_df.loc[:,emi_col], 5 / 100., sort_df.loc[:,'weight'])
    p95 = compute_weighted_percentile(sort_df.loc[:,emi_col], 95 / 100., sort_df.loc[:,'weight'])
    
    ax[1].axvline(p5, color='red', linestyle='dashed', alpha=0.5)
    ax[1].axvline(p95, color='red', linestyle='dashed', alpha=0.5)
    ax[0].axvspan(p5,p95, color='red',alpha=0.2)
    
    
    # In[125]:
    
    
    ax[1].set_ylabel('Quantile')
    ax[1].set_xlabel('Emission Allocations in 2030 (MtCO2eq)')
    
    
    # In[126]:
    
    
    # Add line between min and max 
    ax[1].plot([_min,_max],[0,1],'--b',color='grey',alpha=0.5)
    ax[1].plot([p5,p95],[0.05,0.95],'--b',color='red',alpha=0.5)
    
    
    # In[127]:
    
    
    for n, sax in enumerate(ax):
        sax.text(-0.1, 1.03, string.ascii_lowercase[n], transform=sax.transAxes, 
                size=20, weight='bold')
    
    
    # In[128]:
    
    
    fig.savefig(
        os.path.join(
            '..','figures','s231_fair_share_range_{}.png'.format(iso)
        ),
        dpi=600,
        bbox_inches='tight'
    )


# In[ ]:




