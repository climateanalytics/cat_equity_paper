#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 12:22:04 2021

@author: gauravganti
"""

import pyam 
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib as mpl 
import sys
import os 
import string
from matplotlib.patches import Patch
from matplotlib.lines import Line2D

sys.path.append('..')
from utils import * 

#%% Load Data 

plt.style.use('../climatic_change.mplstyle')

config = read_yaml("config.yaml","..")

# Load configuration file 
equity_data = load_equity_data(config,"..")

# Load mapping file 
mapping = pd.read_csv(
    os.path.join('..','mapping', 'default_mapping.csv')    
).set_index('ISO')

# Load historic data 
historic_data = pd.read_csv(
    os.path.join('..','data','input','Historic_data_per_country_EU27.csv'),
    index_col = 0
)

# IAM SR1.5 data
iam_data = pd.read_excel(
    os.path.join('..','data','iam_r5_data.xlsx')            
)

#%% Statistics at the R5 level 

cases = [
    'mm_line',
    'quant_line',
    'mm_dist'    
]

regions = [
    'R5LAM',
    'R5OECD',
    'R5MAF',
    'R5REF',
    'R5ASIA'    
]

hist_2010 = historic_data['2010']

for i, c in enumerate(cases):
    equity_data[c]['R5_region'] = (
        equity_data[c]
        .index
        .get_level_values('region')
        .map(
            lambda x: mapping.loc[x,'R5_region'] if x in mapping.index else np.nan    
        )
    )
    
    # Obtain the necessary data
    df_1p5 = equity_data[c][
        equity_data[c].index.get_level_values('scenario') == '2100_GMT<1.5C@P0.5'
    ].droplevel('scenario')
    
    for ix in df_1p5.index:
        try:
            df_1p5.loc[ix,'2030_allowances'] = (
                df_1p5.loc[ix,'2030_allowances'] 
                / 
                hist_2010.loc[ix]
                - 
                1) * 100
        except:
            print(ix)
    
    
    _summary = (
            df_1p5
            .groupby(['R5_region'])['2030_allowances']
            .describe(percentiles=[0.1,0.2,0.25,0.5,0.75,0.8,0.9])
    )
    _summary['case'] = c
            
    
    if i==0:
        result = _summary
    else:
        result = result.append(_summary)
    
    df_1p5.to_excel('../tables/{}_country.xlsx'.format(c))

#%% Summary data for the R5 regions
iam_data['reduction'] = (iam_data[2030]/iam_data[2010]-1)*100

iam_summary = iam_data.groupby('region')['reduction'].agg('median')
        
#%% Plotting function
def plot_median_iqr(ax, df,lo='10%',hi='90%'):
    """Helper function to plot median and IQR"""
    ticks = list()
    labels=list()
    
    for i,reg in enumerate(regions):
        
        # Median
        ax.hlines(
            df.loc['50%',reg],
            i - 0.3,
            i + 0.3
        )
        
        # Bar plot
        ax.bar(
            x  = i,
            bottom = df.loc[lo,reg],
            height = df.loc[hi,reg] - df.loc[lo,reg],
            width=0.3,
            alpha=0.3,
            color='grey'
        )
        ticks.append(i)
        labels.append(reg)
        
        if reg == 'R5OECD':
            reg_iam = 'R5OECD90+EU'
        else:
            reg_iam = reg
            
        ax.hlines(
            iam_summary.loc[reg_iam],
            i - 0.3,
            i + 0.3,
            color = 'purple'
        )
    
    ax.set_xticks(ticks)
    ax.set_xticklabels(labels, rotation=55)

#%%
lo = '10%'
hi = '90%'

#%% Define figure

fig, ax = plt.subplots(1,3,sharey=True)

for i, case in enumerate(cases):
    df = result[result.case==case].T
    plot_median_iqr(ax[i],df)
    
ax[0].set_ylabel('Fair Share Emissions in 2030\n(% change compared to 2010 levels)')

# Add annotations for subplots
for i, a in enumerate(ax):
    a.text(-0.1, 1.05, string.ascii_lowercase[i], transform=a.transAxes,
           size = 20, weight='bold')

# Add legend elements 
legend_elements = [
    Line2D([0],[0],color='black',lw=3, label='Median (Fair Share)'),
    Line2D([0],[0], color='purple',lw=3, label='Median (Cost Effective)'),
    Patch(facecolor='grey', alpha=0.3, label='{} - {}'.format(lo,hi))    
]

ax[1].legend(handles = legend_elements, bbox_to_anchor=(1.3,-0.23), ncol=1)

#%% Save figure and associated table 

fig.savefig(
    os.path.join('..','figures','Figure4.png')    
)

result.to_excel(
    os.path.join('..','tables','TableForFigure4.xlsx')    
)

iam_summary.to_excel(
    os.path.join('..','tables','IamTableForFigure4.xlsx')
)