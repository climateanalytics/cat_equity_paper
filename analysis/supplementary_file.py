#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 18:15:43 2021

@author: gauravganti
"""
import pandas as pd 
import matplotlib.pyplot as plt 
import os 
import sys 

sys.path.append('..')
from utils import * 

#%% Load Data
config = read_yaml("config.yaml","..")

# Load configuration file 
equity_data = load_equity_data(config,"..")

#%% Sheet name
sheets = {
    'mm_line':'Min_Max_Linear_Walk',
    'quant_line':'5th_95th_Linear_Walk',
    'mm_dist':'Min_Max_Distribution_Walk'
}

col_order = [
    '2100_GMT<1.5C@P0.5',
    'peak_GMT<2.0C@P0.66',
    'peak_GMT<3.0C@P0.66',
    'peak_GMT<4.0C@P0.66',  
]
#%%

writer = pd.ExcelWriter(
    '../tables/supplement_fair_shares.xlsx',
    engine='xlsxwriter'
)

for frame, sheet in sheets.items():
    _df =  equity_data[frame]['2030_allowances'].reset_index()
    _df = _df.pivot(
        index='region', 
        columns='scenario', 
        values='2030_allowances'
    )[col_order].drop(['EARTH','EUR5OECD','EUR5REF','EU28'])
    _df.to_excel(writer, sheet_name=sheet)

writer.save()