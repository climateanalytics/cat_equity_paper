#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 11 10:21:28 2021

@author: ageiges, gauravganti
"""
# =============================================================================
# Load packages
# =============================================================================
import pyam 
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib as mpl 
import sys
import os 
import string
from matplotlib.patches import Patch
from matplotlib.lines import Line2D

sys.path.append('..')
from utils import * 

#%% Load Data
plt.style.use('../climatic_change.mplstyle')

config = read_yaml("config.yaml","..")

# Load configuration file 
equity_data = load_equity_data(config,"..")

# Load historic data 
df_hist = pd.read_csv(
    os.path.join('..','data','input','Historic_data_per_country_EU27.csv'),
    index_col = 0
)

# Load NDC data
ndc_data = pd.read_excel(
    os.path.join('..','data','input','ndc_data.xlsx'),
    index_col='iso'    
)

#%% Define countries to plot
countries = [
    "EU27",
    "GBR",
    "USA",
    "RUS",
    "BRA",
    "CHN",
    "ZAF",
    "IND"
]

#%% Plot function

def plot_eq_bar(ax, 
                delta, 
                countries, 
                eq_data, 
                df_hist,
                ndc_data,
                scenarios, 
                colors, 
                labels, 
                width,
                hatch,
                relative = True):
    
    for i, coISO in enumerate(countries):
        old_scenario = 'lower'
        mask = (eq_data.region == coISO)
        hi = 'upper'
        eq_data_crop = (eq_data.loc[eq_data.index[mask], :]).set_index('scenario')
        for scenario, color, label in zip(scenarios, colors, labels):
            
            if scenario not in eq_data_crop.index:
                old_scenario = scenario
                continue
            hist_comparison_year = df_hist.loc[coISO, str(comparison_year)]
            bottom = eq_data_crop.loc[old_scenario, str(year) + '_allowances']
            true_upper = eq_data_crop.loc[hi, str(year) + '_allowances']
            top = eq_data_crop.loc[scenario, str(year) + '_allowances']
            if relative:
                hist_comparison_year = df_hist.loc[coISO, str(comparison_year)]
                bottom = (eq_data_crop.loc[old_scenario, str(year) + '_allowances'] / hist_comparison_year - 1) * 100
                true_upper = (eq_data_crop.loc[hi, str(year) + '_allowances'] / hist_comparison_year - 1) * 100
                top = (eq_data_crop.loc[scenario, str(year) + '_allowances']/ hist_comparison_year - 1) * 100
                ax.bar(
                    x = i +delta,
                    # bottom = -(hist_comparison_year - bottom) / hist_comparison_year,
                    # height = -((hist_comparison_year - top) / hist_comparison_year) + ((hist_comparison_year - bottom) / hist_comparison_year),
                    bottom = (bottom),
                    height = top-bottom,
                    width = width,
                    label = label,
                    color = color,
                    alpha = .4,
                    hatch = hatch
                    
                )
               
            else:
                bottom = bottom
                height = top - bottom
                ax.bar(
                    x = i +delta,
                    bottom = bottom,
                    height = height,
                    width = width,
                    label = label,
                    color = color,
                    alpha = .4,
                    hatch = hatch
                )
            
            old_scenario = scenario
            
            if coISO in ndc_data.index:
                for ndc_col, marker in zip(['lo','hi'], ['o', 's']):
                    ndc_value = ndc_data.loc[coISO,ndc_col]
                    relative_value =  (ndc_value / hist_comparison_year-1)*100
                    #plt.plot(
                    #    i+delta, 
                    #    true_upper,
                    #    marker='_', 
                    #    color='black',
                    #    markersize=8,
                    #    alpha=0.5
                    #) # Add upper end of range
                    if (relative_value < top) and (relative_value > bottom) or \
                       (relative_value > top) and (scenario == scenarios[-1]) :
                       
                       
                       if (relative_value > top) and (scenario == scenarios[-1]):
                           color = 'gray'
                       if relative:
                           relative_value =  (ndc_value / hist_comparison_year-1)*100
                           # relative_value =  (hist_comparison_year - ndc_value) / hist_comparison_year
                           plt.plot(i +delta, relative_value, marker, color= color, 
                                    markeredgecolor='k',
                                    markeredgewidth=0.2,
                                    markersize=4)
                       else:
                           plt.plot(i +delta, ndc_value, marker, color= color,
                                    markeredgecolor='k',
                                    markeredgewidth=0.5)
                           
#%% Plot
fig,ax = plt.subplots(num="bar")

ax.cla()
temperatures = [1.5, 2.0, 3.0, 4.0] 
probabilities = [.5, .66, .66, .66]
typ_GMT    = ['2100', 'peak', 'peak', 'peak']
labels = ["<1.5°C", "<2°C", "<3°C", "<4°C"]
colors = ["#B0CD6D", "#f2e56d", "#f2ae52", '#f06b4e']
hatches = [None, '//', '++']
scenarios = [f'{typ}_GMT<{temp:1.1f}C@P{prop}'.format() for typ, temp, prop in zip(typ_GMT, temperatures, probabilities)]

width = .25
relative = True
year = 2030
comparison_year = 2010
plt.rcParams['hatch.linewidth'] = .1

# =============================================================================
# Plot
# =============================================================================
plot_eq_bar(ax, -width*1.1, countries, equity_data['mm_line'].reset_index(), df_hist, ndc_data,scenarios, colors, labels, width, '//', relative)
plot_eq_bar(ax, 0, countries, equity_data['quant_line'].reset_index(), df_hist, ndc_data,scenarios, colors, labels, width, '', relative)
plot_eq_bar(ax, width*1.1, countries, equity_data['mm_dist'].reset_index(), df_hist, ndc_data,scenarios, colors, labels, width, 'xx', relative)

ax.set_ylabel('Fair Share Emissions in 2030\n(% change compared to 2010 levels)')
ax.axhline(0, color='grey', alpha=0.5)

country_map = {
    "EU27":"EU27",
    "GBR":"UK",
    "USA":"USA",
    "RUS":"Russia",
    "BRA":"Brazil",
    "CHN":"China",
    "ZAF":"South Africa",
    "IND":"India"  
}

plt.xticks(range(len(country_map.keys())), labels=country_map.values(), rotation=45)

# =============================================================================
# Legend
# =============================================================================
legend_elements = [
    Line2D([0],[0],color='w',marker='o',markerfacecolor='black',markersize=5, markeredgecolor='k',markeredgewidth=0.2, label='NDC (high)'),
    Line2D([0],[0],color='w',marker='s',markerfacecolor='black',markersize=5, markeredgecolor='k',markeredgewidth=0.2, label='NDC (low)'),
    Patch(facecolor='#B0CD6D', label='1.5°C fair share'),
    Patch(facecolor='#f2e56d', label='2°C fair share'),
    Patch(facecolor='#f2ae52', label='3°C'),
    Patch(facecolor='#f06b4e', label='4°C'),
    Patch(facecolor='grey', hatch='//', label='Min/Max (Linear Walk)', alpha=0.3),
    Patch(facecolor='grey', hatch='', label='5th/95th percentile (Linear Walk)', alpha=0.3),
    Patch(facecolor='grey', hatch='xx', label='Min/Max (Distribution Walk)', alpha=0.3)
]

ax.legend(handles=legend_elements, bbox_to_anchor=(1.2,-0.4), ncol=3)

#%% Save out the figure
fig.savefig(
    os.path.join('..','figures','Figure5.png')    
)
