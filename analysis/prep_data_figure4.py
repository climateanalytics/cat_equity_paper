#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 10:52:24 2021

@author: gauravganti
"""

import pyam 
import pandas as pd 
import os 

#%% Query params

# Define variables to query
variables = [
    'Emissions|Kyoto Gases',
    'Emissions|CO2|AFOLU'    
]

# Define regions
regions = 'R5*'

#%% Query
df = pyam.read_iiasa(
    'iamc15',
    model = '*',
    variable = variables,
    region = regions, 
    meta = ['category']    
)

#%% Filter

# Filter for the year 
df = df.filter(year = [2010,2030], category=['Below 1.5C','1.5C low overshoot'])

# Kyoto gas frame
kyoto = df.filter(
    variable = 'Emissions|Kyoto Gases'
).timeseries().droplevel([3,4])

lulucf = df.filter(
    variable = 'Emissions|CO2|AFOLU'    
).timeseries().droplevel([3,4])

kyoto_excl_lulucf = (kyoto - lulucf).dropna()

#%% Write out to data folder
kyoto_excl_lulucf.to_excel(
    os.path.join('..','data','iam_r5_data.xlsx'),
    merge_cells=False            
)