#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 12:34:35 2021

@author: gauravganti
"""

import yaml
import os 
import pandas as pd

#%% Load yaml

def read_yaml(file, path="."):
    """Function to read yaml file"""
    with open(os.path.join('..',file),'r') as f:
        config = yaml.safe_load(f)
    return config

#%% Data loader

def load_equity_data(config, path="."):
    """Helper function to load equity data"""
    
    # Define data folder 
    data_folder = os.path.join(
        path, 'data', 'cat_output'    
    )
    
    equity_data = dict()
    
    keys = {
        0:'mm_line',
        1:'quant_line',
        2:'mm_dist'
    }
    
    for i,file in enumerate(config['files_to_load']):
        
        file = str(config['date_str']) + '_' + file
        
        _f = os.path.join(
            data_folder, file    
        )
        
        # Load data
        _data = pd.read_excel(
            _f    
        ).set_index(['region','scenario'])
        
        # Save to dictionary
        equity_data[keys[i]] = _data
        
    print('Data loaded successfully')
    
    return equity_data
        